module MyGame {

    export enum Scale
    {
        mood = 1,
        atmosphere,
        literature,
        stakes
    }

    export enum Summand {
        comedy = 1,
        drama = -1,
        action = 2,
        dialog = -2,
        fan = 3,
        new = -3,
        bigStakes = 4,
        lowStakes = -4
    }


	export class Player
    {
        name: string;
        scales: number[];
        rng: Phaser.RandomDataGenerator;

        maximumAllowedValue = 100;
        minimumAllowedValue = 0;
        initialValue = 50;
        stupidDummyValue = 9999;

        moviesProduced = 0;

        deserveNewHero: boolean;

        constructor()
        {
            this.scales = [];
            this.scales.push(this.stupidDummyValue); // filler value for first array index, which we ignore
            for (const scale in Scale) {
                if (!Number(scale)) {
                    this.scales.push(this.initialValue);
                }
            }
            this.rng = new Phaser.RandomDataGenerator([Date.now()]);
            this.deserveNewHero = false;
        }

        castToMultiArrayWithStars(fakeOut: boolean, protagonist: Hero, villain: Hero, ...support: Hero[]): {multiArray: Hero[], stars: Hero[], dangerous: boolean, lowerBoundScales: number[], upperBoundScales: number[]}
        {
            let dangerous = false;
            
            let fullList: Hero[] = [];
            fullList.push(protagonist);
            if (villain != null)
                fullList.push(villain);
            fullList = fullList.concat(support);

            let doubles: Hero[] = [];

            if (fakeOut) // if we fake out the castToMultiArray, we return whether this line-up could maybe kill the player
            {
                let iterationList = fullList.slice(); // loop over a copy to prevent JavaScript confusion
                fullList.push(protagonist);
                let mightDie = false;
                let prognosis = this.processMultiArray(fullList, true);
                let lowerBoundScales = prognosis.theoreticScale.slice();
                let upperBoundScales = prognosis.theoreticScale.slice();
                if (!prognosis.wouldSurvive)
                    mightDie = true;
                // console.log("\n\n\n no stars: " + lowerBoundScales);

                for (let potentialDouble of iterationList)
                {
                    fullList.push(potentialDouble);
                    fullList.push(potentialDouble);
                    fullList.push(potentialDouble);
                    let prognosisWithDouble = this.processMultiArray(fullList, true);
                    if (!prognosisWithDouble.wouldSurvive)
                        mightDie = true;
                    let newScales = prognosisWithDouble.theoreticScale;
                    
                    // console.log("next prognosis: " + newScales);
                    for (let iStr in newScales)
                    {
                        let i = Number(iStr);
                        if (newScales[i] < lowerBoundScales[i]) {
                            lowerBoundScales[i] = newScales[i];
                        }
                        if (newScales[i] > upperBoundScales[i]) {
                            upperBoundScales[i] = newScales[i];
                        }
                    }
                    fullList.pop(); // remove the double again for the next loop iteration
                    fullList.pop();
                    fullList.pop();
                }
                // console.log("lower bounds " + lowerBoundScales);
                // console.log("higher bounds " + upperBoundScales);

                // TODO: tell player if one of their heroes might die due to fatigue!
                return { multiArray: null, stars: null, dangerous: mightDie, lowerBoundScales: lowerBoundScales, upperBoundScales: upperBoundScales};
            }

            //THIS is not a fakeout. Let's count the values
            protagonist.protagonistCount++;
            for (let supportHero of support)
            {
                supportHero.supportCount++;
            }

            if (this.rng.frac() > 0.1) // most of the time the movie will have a star
            {
                doubles.push(this.rng.weightedPick(fullList));
            }
            // you can add another round if you want
            /*
            if (this.rng.frac() > 0.5) {
                double.push(this.rng.weightedPick(fullList));
            }
            */

            fullList.push(protagonist); // protagonist counts at least double
            for (let double of doubles)
            {
                double.starredCount++;
                fullList.push(double);
                fullList.push(double);
                fullList.push(double); // TRIPLE COUNT for the star! too extreme?
                /*
                if (double === protagonist)
                    fullList.push(double); // protagonist star counts four times
                 */ // deactivated! protagonist times four is too strong, especially as the protagonist is most likely to be the star
            }
            console.log("Number of supports " + support.length);
            if (support.length == 3) {
                console.log("We deserve a new hero!");
                this.deserveNewHero = true;
            }
            return { multiArray: fullList, stars: doubles, dangerous: dangerous, lowerBoundScales: [], upperBoundScales: []}; // we do not care about the scales in this scenario
        }

        processMultiArray(multiArray: Hero[], fakeOut: boolean): {wouldSurvive: boolean, theoreticScale: number[]}
        {
            let scales = this.scales;
            if (fakeOut)
                scales = this.scales.slice(); // we work on a fake scale
            for (let hero of multiArray)
            {
                if (!fakeOut)
                    hero.raiseFatigue(); // the more often the character shows up on the multiarray, the more we raise fatigue
                if (hero.checkIsMarkedForDeath())
                    this.effectScale(hero, Scale.mood, false, fakeOut, scales); // the more often the character shows up on the multiarray, the more dramatic the death
                for (let summand of hero.summands)
                {
                    this.effectScale(hero, Math.abs(summand), summand > 0, fakeOut, scales);
                }
            }
            let yetToIgnoreFirstValue = true;


           
            let wouldSurvive = true;
            for (let value of scales)
            {
                if (!yetToIgnoreFirstValue) {
                    if (value < this.minimumAllowedValue)
                        wouldSurvive = false;
                    if (value > this.maximumAllowedValue)
                        wouldSurvive = false;
                }
                else
                    yetToIgnoreFirstValue = false;
            }

            return {wouldSurvive: wouldSurvive, theoreticScale: scales};
        }

        

        effectScale(hero: Hero, scaleIndex: number, doRaise: boolean, fakeOut: boolean, whichScales?: number[])
        {
            let scales = this.scales;
            if (whichScales)
                scales = whichScales;
            if (doRaise) {
                scales[scaleIndex] += 5;
            }
            else {
                scales[scaleIndex] -= 5;
            }
            // if !fakeOut)
            //          animate the effectScale going from hero to scale here.
        }
    }

}