﻿module MyGame {

    export enum Phase
    {
        start,
        alert,
        chooseProtagonist,
        chooseSupport1,
        chooseSupport2,
        chooseSupport3,
        done,
        sendInMovie,
        evaluateMovie,
        newCastings
    }

    export class Match extends Phaser.State {

        background: Phaser.Sprite;
        music: Phaser.Sound;

        currentPhase: Phase;

        scaleGroup: Phaser.Group;
        heroGroup: Phaser.Group;
        villainGroup: Phaser.Group;
        popUpGroup: Phaser.Group;

        scaleX: number = 0;
        scaleY: number = 128;
        scaleXOffset: number = 128;

        heroX: number = 32;
        heroXAlternate: number = 32+256;
        heroY: number = 512+64+64+64;
        heroYOffset: number = 32;

        villainX: number[] = [32, 32 + 128, 32 + 256, 32 + 64, 32 + 128 + 64];
        villainY: number[] = [704+128, 704+128, 704+128, 704+128+128, 0];
        // villainYOffset: number = 64;

        moviePlanX: number = 64;
        moviePlanY: number = 128 + 64 + 64;
        moviePlanXOffset: number = 0;
        moviePlanYOffset: number = 16;

        killOffX: number =  64;

        heroGraphics: { graphic: Phaser.Text, hero: Hero, originalX: number, originalY: number}[];
        villainGraphics: { graphic: Phaser.Text, villain: Hero, originalX: number, originalY: number}[];

        moviePlanGroup: Phaser.Group;

        movieTitleField: Phaser.Text;
        protagonistField: Phaser.Text;
        killProtagonistField: KillField;
        villainField: Phaser.Text;
        supportsFields: Phaser.Text[];
        killSupportsFields: KillField[];
        doneButton: Phaser.Text;

        static ProtagonistCandidate: Hero;
        static VillainCandidate: Hero;
        static SupportRoleCandidate: Hero[];

        achievementWidget: AchievementWidget;
        heroStyle;
        player: Player;
        scaleGraphics: (Phaser.Text | ScaleGraphic)[];

        showPopUpForNewHero: boolean;
        showPopUpForLastHero: boolean;

        heroStack: HeroStack;

        create() {
            this.player = new Player();

            this.scaleGroup = new Phaser.Group(this.game);
            this.game.stage.backgroundColor = '#f3f0ed';
            let outlined = this.game.add.image(0, 0, 'outlined');
            outlined.width = this.game.width;
            outlined.height = this.game.height;
            this.scaleGroup.add(outlined);

            this.currentPhase = Phase.chooseProtagonist;
            this.music = this.add.audio('creditsMusic', 1, true);
            this.music.play();

            this.showPopUpForNewHero = true;
            this.showPopUpForLastHero = true;

            this.scaleY = this.game.height * 0.1;
            this.moviePlanY = this.game.height * 0.2;

            this.heroX = this.game.width * 0.25;
            this.heroXAlternate = this.game.width * 0.75;
            this.heroY = this.game.height * 0.5;

            this.villainX = [this.game.width * 0.25, this.game.width * 0.75, this.game.width * 0.25, this.game.width * 0.75];
            this.villainY = [this.game.height * 0.85, this.game.height * 0.85, this.game.height * 0.85 + 64, this.game.height * 0.85 + 64];

            this.killOffX = this.game.width - 64;

            let fontSize = 12;
            Match.ProtagonistCandidate = null;
            Match.VillainCandidate = null;
            Match.SupportRoleCandidate = [];
            Match.SupportRoleCandidate.push(null);
            Match.SupportRoleCandidate.push(null);
            Match.SupportRoleCandidate.push(null);
            let moviePlanStyle = { font: "36px Delius", stroke: '#FF0000', strokeThickness: 6 };

            this.scaleGraphics = [];
            let scaleLabel = this.game.add.text(this.scaleX, this.scaleY, "", moviePlanStyle);
            let topLabels = ["😆", "💣", "👓", "🌎"];
            let bottomLabels = ["😢", "💬", "🎥", "🌆"];
            let oldScale: Phaser.Text | ScaleGraphic = scaleLabel;
            this.scaleGraphics.push(oldScale);
            for (let i in topLabels) {
                let newScale = new ScaleGraphic(this.game, 2, 3, this.scaleGroup);
                this.scaleGraphics.push(newScale);
                newScale.alignTo(oldScale, Phaser.RIGHT_CENTER, this.scaleXOffset);
                let topText = this.game.add.text(5, 40, topLabels[i], { font: fontSize * 4 + 'px Arial', fill: '#ff0000' }, this.scaleGroup);
                let bottomText = this.game.add.text(5, 40, bottomLabels[i], { font: fontSize * 4 + 'px Arial', fill: '#ff0000' }, this.scaleGroup);
                topText.alignTo(newScale, Phaser.TOP_CENTER);
                bottomText.alignTo(newScale, Phaser.BOTTOM_CENTER);
                oldScale = newScale;
            }

            KillField.ChangeInKillingSignal = new Phaser.Signal();
            KillField.ChangeInKillingSignal.add(this.predictBounds, this);

            this.moviePlanGroup = new Phaser.Group(this.game);

            let heroStyle = { font: "32px Delius", buttonMode: true};

            this.movieTitleField = this.game.add.text(this.moviePlanX, this.moviePlanY+16, "Now CASTING! TAP a HERO", moviePlanStyle, this.moviePlanGroup);
            this.movieTitleField.fontSize = 40;
            this.protagonistField = this.game.add.text(this.moviePlanX, this.moviePlanY, "Protagonist", moviePlanStyle, this.moviePlanGroup);
            this.protagonistField.alignTo(this.movieTitleField, Phaser.BOTTOM_LEFT, undefined, this.moviePlanYOffset);
            this.killProtagonistField = new KillField(this.moviePlanX, this.moviePlanY, heroStyle, this.moviePlanGroup);
            this.moviePlanGroup.add(this.killProtagonistField);
            this.killProtagonistField.alignTo(this.movieTitleField, Phaser.BOTTOM_LEFT, undefined, this.moviePlanYOffset);
            this.killProtagonistField.x = this.killOffX;

            this.killSupportsFields = [];
            this.villainField = this.game.add.text(this.moviePlanX, this.moviePlanY, "Villain", moviePlanStyle, this.moviePlanGroup);
            this.villainField.alignTo(this.protagonistField, Phaser.BOTTOM_LEFT, undefined, this.moviePlanYOffset);
            let supportCount = 3;
            let alignUnder = this.villainField;
            this.supportsFields = [];
            for (let i = 0; i < supportCount; i++) {
                let supportField = this.game.add.text(this.moviePlanX, this.moviePlanY, "Support #" + (i + 1), moviePlanStyle, this.moviePlanGroup);
                this.supportsFields.push(supportField);
                supportField.alignTo(alignUnder, Phaser.BOTTOM_LEFT, 0, this.moviePlanYOffset);
                let killField = new KillField(this.moviePlanX, this.moviePlanY, heroStyle, this.moviePlanGroup);
                this.moviePlanGroup.add(killField);
                killField.alignTo(alignUnder, Phaser.BOTTOM_LEFT, 0, this.moviePlanYOffset);
                killField.x = this.killOffX;
                alignUnder = supportField;
                this.killSupportsFields.push(killField);
            }
            let doneButtonStyle = { font: "32px Delius", fill: "#FFFFFF", buttonMode: true, backgroundColor: "#000000"};
            this.doneButton = this.game.add.text(this.game.width / 2, this.game.height - 48, "choose a hero.", doneButtonStyle, this.moviePlanGroup);
            this.doneButton.anchor.x = 0.5;
            // this.doneButton.alignTo(this.movieTitleFiteld, Phaser.RIGHT);
            this.doneButton.inputEnabled = true;
            let doneSignal: Phaser.Signal = new Phaser.Signal();
            doneSignal.add(() => {
                if (this.currentPhase > Phase.chooseSupport1 && Match.VillainCandidate != null) {
                    let supportCast: Hero[] = this.getCurrentSupportCast();
                    let result = this.player.castToMultiArrayWithStars(false, Match.ProtagonistCandidate, Match.VillainCandidate, ...supportCast);
                    this.player.processMultiArray(result.multiArray, false);
                    this.switchToPhase(Phase.sendInMovie);
                }
            }, this);

            this.doneButton.events.onInputDown.add(() => doneSignal.dispatch(), this);

            
            this.heroStack = new HeroStack(Date.now());
            console.log("New hero stack." + this.heroStack.rng.state());
            
            this.heroStyle = heroStyle;
            this.heroGroup = new Phaser.Group(this.game);
            this.heroGraphics = [];
            // let heroField = this.game.add.text(this.heroX, this.heroY, "Heroes", heroStyle, this.heroGroup);
            for (let hero of this.heroStack.heroes) {
                this.addHero(hero);
            }
            this.rearrangeHeroes();
            this.villainGroup = new Phaser.Group(this.game);
            this.villainGraphics = [];
            // let villainField = this.game.add.text(this.villainX[0], this.villainY[0]- 64, "Villains", heroStyle, this.villainGroup);
            // let oldVillainField = villainField;
            for (let villain of this.heroStack.villains) {
                this.addVillain(villain);
            }
            this.rearrangeVillains();
            // Back
            let backLabel = this.game.add.text(32, 48, 'Retire', {font: 5*2 + 'px Delius'});
            backLabel.x = (this.game.world.width - backLabel.width*2);
            backLabel.inputEnabled = true;
            backLabel.events.onInputUp.add(this.back, this);

            this.achievementWidget = new AchievementWidget(this, 0, 0, false, heroStyle);
            this.popUpGroup = new Phaser.Group(this.game);
            new PopUp(this.game, this.player, this.popUpGroup, "Every movie needs a protagonist hero, a villain and 1-3 support heros. You lose when the top scales get out of control. Before you produce a movie, indicators show where it might put you in the scale. Click on the last cast hero to uncast him/her. Being a producer is very hard. Try again when you fail.", heroStyle, false);
        }

        update()
        {
            let skipFirst = true;
            for (let scaleId in this.scaleGraphics)
            {
                let relevantScale: Phaser.Text | ScaleGraphic = this.scaleGraphics[scaleId];
                if (relevantScale instanceof ScaleGraphic)
                {
                    relevantScale.updateCurrentValue(this.player.scales[scaleId]);
                }
            }

            for (let heroObject of this.heroGraphics)
            {
                // heroObject.graphic.setText(heroObject.);
                if (heroObject.hero.chosenRoleField != null) {
                    heroObject.graphic.alignTo(heroObject.hero.chosenRoleField, Phaser.RIGHT_BOTTOM, this.moviePlanXOffset);
                }
                else
                {
                    heroObject.graphic.x = heroObject.originalX;
                    heroObject.graphic.y = heroObject.originalY;
                }
                // this.scaleGraphics[scaleTextId].text = this.player.scales[scaleTextId];
            }
            for (let villainObject of this.villainGraphics) {
                villainObject.graphic.setText(villainObject.villain.name + " " + villainObject.villain.giveSummandsString());
                if (villainObject.villain.chosenRoleField != null) {
                    villainObject.graphic.alignTo(villainObject.villain.chosenRoleField, Phaser.RIGHT_BOTTOM, this.moviePlanXOffset);
                }
                else {
                    villainObject.graphic.x = villainObject.originalX;
                    villainObject.graphic.y = villainObject.originalY;
                }
            }
        }

        addHero(hero: Hero)
        {
            let heroField = this.game.add.text(this.game.width / 2, this.game.height * 1.2, hero.getHeroStatusField(), this.heroStyle, this.heroGroup);
            heroField.anchor.x = 0.5;
            this.heroGraphics.push({ graphic: heroField, hero: hero, originalX: heroField.x, originalY: heroField.y });
            heroField.inputEnabled = true;
            let clickSignal: Phaser.Signal = new Phaser.Signal();
            clickSignal.add(
                () => {
                    if (hero.hasRole()) {
                        switch (this.currentPhase) {
                            case Phase.chooseSupport1: {
                                if (hero.chosenRoleField == this.protagonistField) {
                                    hero.clearRole();
                                    this.hideScaleBounds();
                                    this.killProtagonistField.hide();
                                    this.switchToPhase(Phase.chooseProtagonist);
                                }
                                break;
                            }
                            case Phase.chooseSupport2: {
                                if (hero.chosenRoleField == this.supportsFields[0]) {
                                    hero.clearRole();
                                    this.killSupportsFields[0].hide();
                                    this.switchToPhase(Phase.chooseSupport1);
                                }
                                break;
                            }
                            case Phase.chooseSupport3: {
                                if (hero.chosenRoleField == this.supportsFields[1]) {
                                    hero.clearRole();
                                    this.killSupportsFields[1].hide();
                                    this.switchToPhase(Phase.chooseSupport2);
                                }
                                break;
                            }
                            case Phase.done: {
                                if (hero.chosenRoleField == this.supportsFields[2]) {
                                    hero.clearRole();
                                    this.killSupportsFields[2].hide();
                                    this.switchToPhase(Phase.chooseSupport3);
                                }
                                break;
                            }
                            default: {
                                break;
                            }
                        }
                    }
                    else {
                        switch (this.currentPhase) {
                            case Phase.chooseProtagonist: {
                                hero.giveRole(this.protagonistField, Role.protagonist);
                                this.killProtagonistField.show(hero);
                                this.switchToPhase(Phase.chooseSupport1);
                                break;
                            }
                            case Phase.chooseSupport1: {
                                hero.giveRole(this.supportsFields[0], Role.support1);
                                this.killSupportsFields[0].show(hero);
                                this.switchToPhase(Phase.chooseSupport2);
                                break;
                            }
                            case Phase.chooseSupport2: {
                                hero.giveRole(this.supportsFields[1], Role.support2);
                                this.killSupportsFields[1].show(hero);
                                this.switchToPhase(Phase.chooseSupport3);
                                break;
                            }
                            case Phase.chooseSupport3: {
                                hero.giveRole(this.supportsFields[2], Role.support3);
                                this.killSupportsFields[2].show(hero);
                                this.switchToPhase(Phase.done);
                                break;
                            }
                            default: {
                                break;
                            }
                        }

                    }

                }, this);
            heroField.events.onInputDown.add(() => clickSignal.dispatch());
        }

        addVillain(villain: Hero) {
            let villainField = this.game.add.text(-this.game.width * 0.2, this.game.height * 1.2, villain.name + " " + villain.giveSummandsString(), this.heroStyle, this.villainGroup);
            this.villainGraphics.push({ graphic: villainField, villain: villain, originalX: villainField.x, originalY: villainField.y });
            villainField.anchor.x = 0.5;
            villainField.inputEnabled = true;
            let clickSignal: Phaser.Signal = new Phaser.Signal();
            clickSignal.add(
                () => {
                    if (!villain.hasRole()) {
                        if (Match.VillainCandidate != null)
                            Match.VillainCandidate.clearRole();
                        villain.giveRole(this.villainField, Role.villain);
                        this.predictBounds();
                        if (Match.SupportRoleCandidate[0] != null) {
                            this.doneButton.text = "click me to produce";
                            this.game.add.tween(this.doneButton).to({ x: this.game.width / 2 + 32 }, 2000, Phaser.Easing.Bounce.InOut, true, undefined, 1, true);

                        }
                    }

                }, this);
            villainField.events.onInputDown.add(() => clickSignal.dispatch());
        }

        rearrangeHeroes()
        {
            let latestHeroId = 1;
            for (let heroObject of this.heroGraphics) {
                heroObject.graphic.text = heroObject.hero.getHeroStatusField();
                if (!(heroObject.hero.retired || heroObject.hero.hasRole())) {
                    let futureHeroX = this.heroX;
                    if (latestHeroId % 2 == 0)
                        futureHeroX = this.heroXAlternate;
                    if (heroObject.hero.entryAnimationState == 1)
                    {
                        console.log("New hero entering");
                        heroObject.hero.entryAnimationState = 2;
                        let entryTween = this.game.add.tween(heroObject.graphic).to({
                            x: futureHeroX,
                            y: this.heroY + this.heroYOffset * latestHeroId
                        }, 2000, Phaser.Easing.Bounce.Out, true);
                        entryTween.onComplete.add((graphic) => { console.log("entering happened for" + heroObject.hero.name); heroObject.hero.entryAnimationState = 0; this.rearrangeHeroes(); });
                    }
                    else if (heroObject.hero.entryAnimationState == 0)
                    {
                        heroObject.graphic.x = futureHeroX;
                        heroObject.graphic.y = this.heroY + this.heroYOffset * latestHeroId;
                        heroObject.originalX = heroObject.graphic.x;
                        heroObject.originalY = heroObject.graphic.y;
                    }
                    latestHeroId++;
                    }
            }
        }

        rearrangeVillains() {
            let latestVillainId = 0;
            console.log("started rearranging");
            for (let villainObject of this.villainGraphics) {
                if (!(villainObject.villain.retired || villainObject.villain.hasRole())) {
                    console.log("used villain id " + latestVillainId);
                    if (villainObject.villain.entryAnimationState == 1) {
                        villainObject.villain.entryAnimationState = 2;
                        console.log("New villain entering");
                        let entryTween = this.game.add.tween(villainObject.graphic).to({
                            x: this.villainX[latestVillainId],
                            y: this.villainY[latestVillainId]
                        }, 2500, Phaser.Easing.Bounce.Out, true);
                        entryTween.onComplete.add((graphic) => { villainObject.villain.entryAnimationState = 0; this.rearrangeVillains() });
                    }
                    else if (villainObject.villain.entryAnimationState == 0) {
                        
                        villainObject.graphic.x = this.villainX[latestVillainId];
                        villainObject.graphic.y = this.villainY[latestVillainId];
                        villainObject.originalX = villainObject.graphic.x;
                        villainObject.originalY = villainObject.graphic.y;
                    }
                    latestVillainId++;
                }
            }
        }


        getCurrentSupportCast(): Hero[]
        {
            let supportCast: Hero[] = [];
            if (Match.SupportRoleCandidate[0] != null)
                supportCast.push(Match.SupportRoleCandidate[0]);
            if (Match.SupportRoleCandidate[1] != null)
                supportCast.push(Match.SupportRoleCandidate[1]);
            if (Match.SupportRoleCandidate[2] != null) {
                supportCast.push(Match.SupportRoleCandidate[2]);
            }
            return supportCast;
        }


        predictBounds()
        {
            let supportCast: Hero[] = this.getCurrentSupportCast();
            let prognosis = this.player.castToMultiArrayWithStars(true, Match.ProtagonistCandidate, Match.VillainCandidate, ...supportCast);
            // a new cast member has just been chosen or removed. Let's evaluate the bounds.
            for (let iStr in this.scaleGraphics) {
                let i = Number(iStr);
                let scale = this.scaleGraphics[i];
                if (scale instanceof ScaleGraphic) {
                    scale.updateBounds(prognosis.lowerBoundScales[i], prognosis.upperBoundScales[i]);
                }
            }
            this.movieTitleField.text = Match.ProtagonistCandidate.name + " " + String(Match.ProtagonistCandidate.protagonistCount+1);
        }

        switchToPhase(phase: Phase)
        {
            this.currentPhase = phase;
            if (phase == Phase.chooseProtagonist)
            {
                this.movieTitleField.text = "Choose the Protagonist of Movie #" + (this.player.moviesProduced+1);
                this.doneButton.text = "Tap a hero to choose protagonist.";
            }

            if (phase >= Phase.chooseSupport1 && phase <= Phase.done)
            {
                if (Match.SupportRoleCandidate[0] == null || Match.VillainCandidate == null)
                    this.doneButton.text = "tap 1-3 heroes for support, and a villain";
                else {
                    this.doneButton.text = "Click me to produce!";
                    this.game.add.tween(this.doneButton).to({ x: this.game.width/2 + 32 }, 2000, Phaser.Easing.Bounce.InOut, true, undefined, 1, true);
                }
                this.predictBounds();
            }

            if (phase == Phase.sendInMovie)
            {
                this.player.moviesProduced++;
                let featProtagonist = Match.ProtagonistCandidate;

                let featVillain = Match.VillainCandidate;
                let featSupport = this.getCurrentSupportCast();
                for (let heroObject of this.heroGraphics) {
                    let hero = heroObject.hero;
                    if (!hero.retired) {
                        let retired = hero.checkIfRetireRequiredPostMovie();
                        if (retired.didRetire) {

                            heroObject.graphic.inputEnabled = false;
                            let exitTween = this.game.add.tween(heroObject.graphic).to({ x: this.game.width * 1.2, y: this.game.height * 1.2 }, 2000, Phaser.Easing.Bounce.Out, true);
                            exitTween.onComplete.add((graphic) => {
                                graphic.alpha = 0;
                                if (retired.reason != "")
                                    new PopUp(this.game, this.player, this.popUpGroup, retired.reason, this.heroStyle, false);
                                });
                        }
                    }
                    hero.clearRole();
                }

                this.killProtagonistField.hide();
                for (let ksf of this.killSupportsFields)
                {
                    ksf.hide();
                }

                for (let villainObject of this.villainGraphics) {
                    let villain = villainObject.villain;
                    if (!villain.retired)
                    {
                        let retired = villain.checkIfRetireRequiredPostMovie();
                        if (retired.didRetire) {
                            villainObject.graphic.inputEnabled = false;
                            let exitTween = this.game.add.tween(villainObject.graphic).to({x: this.game.width * 1.2, y: this.game.height * 1.2 }, 2000, Phaser.Easing.Bounce.Out, true);
                            exitTween.onComplete.add((graphic) => graphic.alpha = 0);
                        }
                    }
                    villainObject.villain.clearRole();
                }

                this.achievementWidget.checkForUnlocks(this.player, featProtagonist, featVillain, ...featSupport);

                this.hideScaleBounds();
                this.switchToPhase(Phase.newCastings);
            }
            else if (this.currentPhase == Phase.newCastings)
            {
                let lostDueToScales = false;
                let failureMessage = "";
                for (let scaleIdStr in this.player.scales)
                {
                    let scaleId = Number(scaleIdStr);
                    let scale = this.player.scales[scaleId];
                    if (scale != this.player.stupidDummyValue)
                    {
                        let topLabels = ["😆", "💣", "👓", "🌎"];
                        let bottomLabels = ["😢", "💬", "🎥", "🌆"];   
                        if (scale >= this.player.maximumAllowedValue) {
                            lostDueToScales = true;
                            if (scaleId == Scale.mood)
                                failureMessage = "Your movies were too much of a comedy 😆. The audience did not take them serious. Try killing off characters next time 😢.";
                            if (scaleId == Scale.atmosphere)
                                failureMessage = "Your movies had too much action 💣. The budget got too high and you were fired. Get more dialog 💬."
                            if (scaleId == Scale.literature)
                                failureMessage = "Your movies were too close to the source material 👓. Casual fans stopped going, hardcore fans kept reading at home. Appeal to movie goers 🎥.";
                            if (scaleId == Scale.stakes)
                                failureMessage = "Your movies had too high stakes 🌎 all the time. Push it back to the street level on occasion 🌆.";
                        }
                        if (scale <= this.player.minimumAllowedValue) {
                            lostDueToScales = true;
                            if (scaleId == Scale.mood)
                                failureMessage = "Your movies were too dramatic 😢, Zach. The audience wants to laugh 😆 once in a while in a superhero movie.";
                            if (scaleId == Scale.atmosphere)
                                failureMessage = "Your movies were all dialog 💬. While corporate enjoyed the low budgets, the audience stopped showing. Put some action 💣 in."
                            if (scaleId == Scale.literature)
                                failureMessage = "You lost their comic roots and made too trite movies 🎥. Put in things close to the source 👓. Comic book fans stopped endorsing them.";
                            if (scaleId == Scale.stakes)
                                failureMessage = "Your movies had too low stakes 🌆. People stopped caring. Let them fight for the world 🌎 some time.";
                        }
                    }
                }
                if (lostDueToScales)
                {
                    new PopUp(this.game, this.player, this.popUpGroup, failureMessage, this.heroStyle, true);
                }
                if (this.player.deserveNewHero)
                {
                    let newHero = this.heroStack.castRandomHero(false);
                    if (newHero != null) {
                        this.addHero(newHero);
                        if (this.showPopUpForNewHero)
                            new PopUp(this.game, this.player, this.popUpGroup, "Everytime you make a movie with 5 characters, a new hero gets cast. " + newHero.name + " joined this time.", this.heroStyle, false);
                        this.showPopUpForNewHero = false;
                        newHero.entryAnimationState = 1;
                    }
                    else {
                        if (this.showPopUpForLastHero)
                            new PopUp(this.game, this.player, this.popUpGroup, "The entire backlog of heroes has been cast for your movies! No more new heroes.", this.heroStyle, false);
                        this.showPopUpForLastHero = false;
                    }
                    this.player.deserveNewHero = false;
                }
                let availableHeroes = 0;
                for (let hero of this.heroStack.heroes)
                {
                    if (!hero.retired)
                        availableHeroes++;
                }
                if (availableHeroes <= 1)
                    new PopUp(this.game, this.player, this.popUpGroup, "You did not have enough heroes for a movie. Did you know that you get a new hero when you make a movie with 5 characters?", this.heroStyle, true);

                let newVillain = this.heroStack.castRandomHero(true);
                if (newVillain != null)
                {
                    this.addVillain(newVillain);
                    newVillain.entryAnimationState = 1;
                }

                let availableVillains = 0;
                for (let villain of this.heroStack.villains) {
                    if (!villain.retired)
                        availableVillains++;
                }
                if (availableVillains == 0)
                    new PopUp(this.game, this.player, this.popUpGroup, "A galaxy at peace! The entire pantheon of villains has been defeated by your heroes, and the audience was there. You won! Did you beat all achievements yet?", this.heroStyle, true);

                    
                this.rearrangeHeroes();
                this.rearrangeVillains();
                this.switchToPhase(Phase.chooseProtagonist);
            }
        }

        hideScaleBounds()
        {
            for (let iStr in this.scaleGraphics) {
                let i = Number(iStr);
                let scale = this.scaleGraphics[i];
                if (scale instanceof ScaleGraphic) {
                    scale.hideBounds();
                }
            }
        }

        back() {
            this.music.stop();
            this.game.state.start('MainMenu');
        }
    }

    export class KillField extends Phaser.Text
    {
        connectedHero: Hero;
        static ChangeInKillingSignal: Phaser.Signal;
        constructor(x: number, y: number, style, group: Phaser.Group)
        {
            super(group.game, x, y, "kill?", style);
            // group.add(this);
            this.inputEnabled = true;
            this.events.onInputDown.add(this.reactToClick, this);
            this.hide();
        }

        hide()
        {
            this.visible = false;
            this.inputEnabled = false;
            this.setText("kill?", true);
        }

        reactToClick()
        {
            console.log("Clicked the kill-off button");
            if (this.connectedHero != null)
            {
                if (this.connectedHero.checkIsMarkedForDeath())
                {
                    this.connectedHero.unmarkForDeath();
                    this.setText("kill?", true);
                    KillField.ChangeInKillingSignal.dispatch();
                }
                else
                {
                    this.connectedHero.markForDeath();
                    this.setText("💀😢", true);
                    KillField.ChangeInKillingSignal.dispatch();
                }
            }
        }

        show(connectToHero: Hero)
        {
            this.inputEnabled = true;
            this.connectedHero = connectToHero;
            this.visible = true;
        }

    }

    export class PopUp extends Phaser.Sprite
    {
        text: Phaser.Text;
        youLost: boolean;
        constructor(game: Phaser.Game, player: Player, group: Phaser.Group, message: string, style, youLost: boolean) {
            super(game, 0, 0, 'background');
            
            this.width = game.width;
            this.height = game.height;
            this.bringToTop();
            group.add(this);
            if (youLost)
                message = message + "\n" + "But you did produce " + player.moviesProduced + " movies!\nCheck out the achievements.";
            this.text = new Phaser.Text(this.game, game.width * 0.25, game.height * 0.25, message, style);
            this.text.wordWrap = true;
            this.text.wordWrapWidth = game.width * 0.5;
            this.text.bringToTop();
            group.add(this.text);
            this.inputEnabled = true;
            group.add(this);
            this.youLost = youLost;
            this.events.onInputDown.addOnce(this.clicked, this);
        }

        clicked()
        {
            if (this.youLost)
            {
                this.game.state.start('MainMenu');
            }
            let tween = this.game.add.tween(this).to({ x: -this.game.width}, 1500, undefined, true);
            let tween2 = this.game.add.tween(this.text).to({ x: -this.game.width}, 1500, undefined, true);
            tween.onComplete.add(this.destroyFully, this)
        }

        destroyFully()
        {
            this.text.destroy();
            this.destroy();
        }
    }

    export class ScaleGraphic extends Phaser.Sprite
    {
        currentValue: number;
        upperBound: number;
        lowerBound: number;
        showBounds: boolean;

        currentArrow: Phaser.Sprite;
        upperBoundArrow: Phaser.Sprite;
        lowerBoundArrow: Phaser.Sprite;

        constructor(game: Phaser.Game, x: number, y: number, group: Phaser.Group)
        {
            super(game, x, y, 'scale_bar')
            group.add(this);
            this.currentValue = 50;
            this.showBounds = false;

            this.upperBoundArrow = new Phaser.Sprite(this.game, x, y, 'small_arrow_up');
            this.upperBoundArrow.anchor.y = 0.5;
            group.add(this.upperBoundArrow);

            this.lowerBoundArrow = new Phaser.Sprite(this.game, x, y, 'small_arrow_down');
            group.add(this.lowerBoundArrow);
            this.lowerBoundArrow.anchor.y = 0.5;
            this.hideBounds();

            this.currentArrow = new Phaser.Sprite(this.game, x, y, 'small_arrow');
            group.add(this.currentArrow);
            this.currentArrow.anchor.y = 0.5;
            this.moveArrow(this.currentArrow, 50);
        }

        moveArrow(whichOne: Phaser.Sprite, newValue: number)
        {
            if (whichOne == this.currentArrow)
                whichOne.x = this.x - whichOne.width/4;
            if (whichOne == this.lowerBoundArrow)
                whichOne.x = this.x - whichOne.width * 1.25;
            if (whichOne == this.upperBoundArrow)
                whichOne.x = this.x + whichOne.width * 1.25 - this.width; 
            whichOne.y = this.y + 100 - newValue;
            whichOne.update();
        }

        updateCurrentValue(newValue: number)
        {
            this.currentValue = newValue;
            this.moveArrow(this.currentArrow, newValue);
        }

        hideBounds()
        {
            this.upperBoundArrow.alpha = 0;
            this.lowerBoundArrow.alpha = 0;
        }

        updateBounds(lowerBound: number, upperBound: number)
        {
            if (lowerBound != upperBound) {
                this.upperBoundArrow.alpha = 1;
                this.lowerBoundArrow.alpha = 1;
                this.lowerBound = lowerBound;
                this.upperBound = upperBound;
                this.moveArrow(this.lowerBoundArrow, lowerBound);
                this.moveArrow(this.upperBoundArrow, upperBound);
            }
            else
                this.hideBounds();
            
        }

    }

}