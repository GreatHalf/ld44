module MyGame {

	export class Boot extends Phaser.State {

        init() {
            console.log("Initialize Boot state");
			//  Unless you specifically need to support multitouch I would recommend setting this to 1
			this.input.maxPointers = 1;

			//  Phaser will automatically pause if the browser tab the game is in loses focus. You can disable that here:
			this.stage.disableVisibilityChange = true;

			// Enable physics
			//this.game.physics.startSystem(Phaser.Physics.ARCADE);

			if (this.game.device.desktop) {
				//  If you have any desktop specific settings, they can go in here
				this.scale.pageAlignVertically = true;
			}
			else {
				//  Same goes for mobile settings.
				//  In this case we're saying "scale the game, no lower than 480x260 and no higher than 1024x768"
				this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
				this.scale.setMinMax(360, 640, 720, 1280);
				this.scale.forcePortrait = true;
				this.scale.pageAlignVertically = true;
			}
		}

		preload() {
			this.load.image('preloadBar', 'assets/loader.png');
		}

		create() {
			//  By this point the preloader assets have loaded to the cache, we've set the game settings
			//  So now let's start the real preloader going
            console.log("Create Boot state");
            this.game.state.start('Preloader');
		}
	}
}
