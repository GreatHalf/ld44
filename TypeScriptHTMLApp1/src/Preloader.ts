module MyGame {
   

	export class Preloader extends Phaser.State {

		preloadBar: Phaser.Sprite;
		background: Phaser.Sprite;
		ready: boolean = false;

		preload() {

			//	These are the assets we loaded in Boot.js
			this.preloadBar = this.add.sprite(300, 400, 'preloadBar');

			//	This sets the preloadBar sprite as a loader sprite.
			//	What that does is automatically crop the sprite from 0 to full-width
			//	as the files below are loaded in.
			this.load.setPreloadSprite(this.preloadBar);

			//	Here we load the rest of the assets our game needs.

            // this.load.bitmapFont('newFont', 'assets/font/font.png', 'assets/font/font.fnt'); // VTC Letterer Pro.

            let WebFontConfig = {

                //  'active' means all requested fonts have finished loading
                //  We set a 1 second delay before calling 'createText'.
                //  For some reason if we don't the browser cannot render the text the first time it's created.
                active: function () { this.game.time.events.add(Phaser.Timer.SECOND, this.createText, this); },

                //  The Google Fonts we want to load (specify as many as you like in the array)
                google: {
                    families: ['Delius']
                }

            };
            this.game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js')

            AchievementManager.FILL();
			this.load.audio('menuMusic', 'assets/menu.mp3', true);
            this.load.audio('gameMusic', 'assets/game.mp3', true);
            this.load.audio('creditsMusic', 'assets/credits.mp3', true);

            this.load.image('background', 'assets/background.png');
            this.load.image('title', 'assets/main_title.png');
            this.load.image('start', 'assets/main_start.png');
            this.load.image('credits', 'assets/main_credits.png');
            this.load.image('main_ach', 'assets/main_ach.png');

            this.load.spritesheet('solved', 'assets/bag_cell_both.png', 64, 64);

            this.load.image('arrow', 'assets/arrow_right.png');

            this.load.image('outlined', 'assets/outlined2.png');

            this.load.image('scale_bar', 'assets/scale_bar.png');
            this.load.image('small_arrow', 'assets/small_arrow.png');
            this.load.image('small_arrow_down', 'assets/small_arrow_down.png');
            this.load.image('small_arrow_up', 'assets/small_arrow_up.png');
            	}

		create() {
			this.game.state.start('MainMenu');
            this.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
		}
	}
}