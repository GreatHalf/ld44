module MyGame {

    export abstract class Widget {
        x: number;
        y: number;
        notifications: string[];
        protected isUnfolded: boolean; // is this widget active or hiding in a corner?
        foldedGroup: Phaser.Group;
        unfoldedGroup: Phaser.Group;

        constructor(state: Phaser.State, x: number, y: number, startUnfolded: boolean) {
            this.x = x;
            this.y = y;
            this.foldedGroup = state.add.group();
            this.foldedGroup.interactive = true;
            this.unfoldedGroup = state.add.group();
            this.notifications = [];
            if (startUnfolded)
                this.unfold();
            else
                this.fold();
        }

        update() {

        }

        toggleFold() {
            console.log("You toggled the fold on a widget.");
            if (this.isUnfolded)
                this.fold();
            else
                this.unfold();
        }

        abstract addNotification(content: string);

        abstract clearNotifications();

        unfold() {
            this.unfoldedGroup.visible = true;
            this.unfoldedGroup.interactive = true;
            this.isUnfolded = true;
            // this.foldedGroup.forEach(function (sprite) { this.bringToTop(sprite); }, this, true);
        }

        fold() {
            this.unfoldedGroup.visible = false;
            this.unfoldedGroup.interactive = false;
            this.isUnfolded = false;
            this.clearNotifications();
        }
    }

    export class AchievementWidget extends Widget
    {
        unfoldText: Phaser.Text;
        fullyInitialized: boolean = false;
        achievementTexts: Phaser.Text[];
        game: Phaser.Game;
        checkboxes: { image: Phaser.Image, id: string}[];
        originalX: number;
        constructor(state: Phaser.State, x: number, y: number, startUnfolded: boolean, style) {
            super(state, x, y, startUnfolded);
            this.originalX = x;
            this.game = state.game;
            this.unfoldText = state.add.text(x, y, this.getDefaultUnfolderText(), style, this.foldedGroup);
            this.unfoldText.inputEnabled = true;
            this.unfoldText.interactive = true;
            this.unfoldText.events.onInputDown.add(this.toggleFold, this);

            let background = new Phaser.Image(state.game, 0, this.y + this.unfoldText.height, 'background');
            background.width = this.game.width;
            background.height = this.game.height;


            this.unfoldedGroup.add(background);
            this.generateAchievementTexts(x, state.game.height / 4);
            let achievementStartY = this.y + this.unfoldText.height;
            this.checkboxes = [];
            for (let achievementId in this.achievementTexts) {
                this.achievementTexts[achievementId].y = achievementStartY + Number(achievementId) * 84;
                let frame = 0;
                if (AchievementManager.LIST[achievementId].solved)
                    frame = 1;
                let checkbox = new Phaser.Image(state.game, state.game.width - 240  + 128, achievementStartY + Number(achievementId) * 84 + 4, 'solved', frame);
                this.unfoldedGroup.add(checkbox);
                this.checkboxes.push({image: checkbox, id: achievementId})
            }
            
            this.fullyInitialized = true;
        }

        checkForUnlocks(player: Player, protagonist: Hero, villain: Hero, ...support: Hero[])
        {
            let anySolve = false;
            for (let achievementId in AchievementManager.LIST)
            {
                let newSolve = AchievementManager.LIST[achievementId].checkToSolve(player, protagonist, villain, ...support);
                if (newSolve)
                {
                    anySolve = true;
                    this.addNotification("UNLOCKED ACHIEVEMENT! tap this");
                    for (let checkbox of this.checkboxes)
                    {
                        if (checkbox.id == achievementId) {
                            checkbox.image.frame = 1;
                            checkbox.image.update();
                        }
                    }
                }
            }
            if (anySolve)
            {
                AchievementManager.SAVE_ACHIEVEMENTS();
            }
        }

        addNotification(content: string) {
            this.notifications.push(content);
            this.unfoldText.setText(content);
            this.game.add.tween(this.unfoldText).to({ x: this.originalX + 32}, 2000, Phaser.Easing.Bounce.InOut, true, undefined, 1, true);
        }

        getDefaultUnfolderText(): string {
            let unfolderString = "achievements";
            return unfolderString;
        }

        clearNotifications() {
            if (this.fullyInitialized) //otherwise the folding of our super class constructor will call this and run into trouble.
                this.unfoldText.setText(this.getDefaultUnfolderText());
            this.notifications = [];
        }

        generateAchievementTexts(startX: number, startY: number) {
            this.achievementTexts = [];
            for (let achievement of AchievementManager.LIST) {
                let newText = new Phaser.Text(this.game, startX, startY, achievement.name.toUpperCase() + "\n     " + achievement.description.toLowerCase(), { font: "18pt Delius", fill: "#000000" });
                this.achievementTexts.push(newText);
                this.unfoldedGroup.add(newText);
            }
        }
    }

}