﻿module MyGame {

    export enum Role
    {
        villain = -2,
        protagonist = -1,
        support1,
        support2,
        support3
    }

    export enum SpecialHero
    {
        nothing,
        notBird,
        notBirdJr,
        smashPotato,
        darkPotato,
        theOrphan,
        minute,
        hour
    }

	export class Hero
    {
        name: string;
        spriteKey: string;
        isVillain: boolean;
        summands: Summand[];
        private inMovie: boolean;
        entryAnimationState: number;
        chosenRoleField: Phaser.Text;
        retired: boolean;
        protagonistCount: number = 0;
        supportCount: number = 0;
        starredCount: number = 0;

        specialHero: SpecialHero = SpecialHero.nothing;
        private role: Role;
        private fatigue: number; // if this goes to 100, character fatigue sets in. If it goes to 0, the character fades into obscurity
        private isMarkedForDeath: boolean; // if a hero is marked for death (villains can't be), it makes the movie more dramatic

        constructor(name: string, spriteKey: string, isVillain: boolean, ...summands: Summand[])
        {
            this.name = name;
            this.spriteKey = spriteKey;
            this.isVillain = isVillain;
            this.summands = summands;
            this.fatigue = 45;
            this.isMarkedForDeath = false;
            this.retired = false;
            this.entryAnimationState = 0;
        }

        clearRole()
        {
            this.inMovie = false;
            if (!this.retired)
                this.unmarkForDeath(); // no need to unmark for death if the character retired any way
            if (this.role == Role.protagonist)
                Match.ProtagonistCandidate = null;
            else if (this.isVillain)
                Match.VillainCandidate = null;
            else
                Match.SupportRoleCandidate[this.role] = null;
            this.chosenRoleField = null;
        }

        hasRole(): boolean
        {
            return this.inMovie;
        }

        getFatigueIcon(): string {
            if (this.fatigue <= 5)
            {
                return "💤💤💤";
            }
            if (this.fatigue <= 25)
            {
                return "💤💤";
            }
            if (this.fatigue <= 40)
            {
                return "💤";
            }
            if (this.fatigue <= 50)
                return "";
            if (this.fatigue <= 65)
                return "💦";
            if (this.fatigue <= 75) 
                return "💦💦";
            return "💦💦💦";
        }

        getHeroStatusField()
        {
            return this.getFatigueIcon() + this.name + this.giveSummandsString();
        }

        giveRole(field: Phaser.Text, role: Role)
        {
            this.inMovie = true;
            this.role = role;
            if (this.role == Role.protagonist)
                Match.ProtagonistCandidate = this;
            else if (this.role == Role.villain)
                Match.VillainCandidate = this
            else
                Match.SupportRoleCandidate[this.role] = this;
            this.chosenRoleField = field;
        }

        markForDeath()
        {
            this.isMarkedForDeath = true;
        }

        unmarkForDeath()
        {
            this.isMarkedForDeath = false;
        }

        checkIsMarkedForDeath(): boolean
        {
            return this.isMarkedForDeath;
        }

        checkIfRetireRequiredPostMovie(): { didRetire: boolean, reason: string } {
            let reason = "";
            if (this.checkIsMarkedForDeath()) {
                this.retire();
            }
            if (!this.inMovie)
                this.lowerFatigue();
            if (this.fatigue <= 0) {
                this.retire();
                reason = this.name + "'s actor was too bored and left the studio.";
                }
            if (this.fatigue >= 100) {
                this.retire();
                reason = this.name + "'s actor had to work too often and left the studio.";
            }
            if (this.inMovie && this.isVillain) // villains are one-shot
            {
                console.log("villain leaves " + this.name);
                this.retire();
            }
            return { didRetire: this.retired, reason: reason };
        }

        giveSummandsString()
        {
            let output = "";
            for (let summand of this.summands)
            {
                output += this.summandToString(summand);
            }
            return output;
        }

        summandToString(summand: Summand) : string
        {
            switch (summand)
            {
                case Summand.comedy: return "😆";
                case Summand.drama: return "😢";
                case Summand.action: return "💣";
                case Summand.dialog: return "💬";
                case Summand.fan: return "👓";
                case Summand.new: return "🎥";
                case Summand.bigStakes: return "🌎";
                case Summand.lowStakes: return "🌆";
            }
        }

        retire()
        {
            this.retired = true;
            
        }

        getFatigue()
        {
            return this.fatigue;
        }

        raiseFatigue()
        {
            if (!this.isVillain)
                this.fatigue += 10;
        }

        lowerFatigue()
        {
            if (!this.isVillain)
                this.fatigue -= 5;
        }
    }

    export class HeroStack
    {
        heroesUncasted: Hero[];
        villainsUncasted: Hero[];
        heroes: Hero[];
        villains: Hero[];

        initialHeroes = 5;
        initialVillains = 4;

        rng: Phaser.RandomDataGenerator;

        constructor(date: number)
        {
            this.heroesUncasted = [];
            this.heroesUncasted.push(new Hero('Smash Potato', "smash_potato", false, Summand.action, Summand.action));
            this.heroesUncasted[this.heroesUncasted.length - 1].specialHero = SpecialHero.smashPotato
            this.heroesUncasted.push(new Hero('The Orphan', "the_orphan", false, Summand.drama, Summand.drama));
            this.heroesUncasted[this.heroesUncasted.length - 1].specialHero = SpecialHero.theOrphan;
            this.heroesUncasted.push(new Hero('Street Punch', "street_punch", false, Summand.lowStakes, Summand.lowStakes));
            this.heroesUncasted.push(new Hero('The Rhetoric', "the_rhetoric", false, Summand.dialog, Summand.dialog));
            this.heroesUncasted.push(new Hero("Ferret Boy", 'ferret_boy', false, Summand.comedy, Summand.comedy));
            this.heroesUncasted.push(new Hero('Brooklyn Blue', "brooklyn_blue", false, Summand.bigStakes, Summand.bigStakes));
            this.heroesUncasted.push(new Hero("Jarb Jarhead", 'j_jarhead', false, Summand.new, Summand.new));
            this.heroesUncasted.push(new Hero("Shiftmask", 'shiftmask', false, Summand.fan, Summand.fan));

            this.heroesUncasted.push(new Hero('The Not-Bird', "the_not-bird", false, Summand.action, Summand.fan));
            this.heroesUncasted[this.heroesUncasted.length - 1].specialHero = SpecialHero.notBird;
            this.heroesUncasted.push(new Hero('Not-Bird Jr.', "the_not-bird_jr", false, Summand.action, Summand.new));
            this.heroesUncasted[this.heroesUncasted.length - 1].specialHero = SpecialHero.notBirdJr;
            this.heroesUncasted.push(new Hero('Hour Hand', "hour_hand", false, Summand.comedy, Summand.bigStakes));
            this.heroesUncasted[this.heroesUncasted.length - 1].specialHero = SpecialHero.hour;
            this.heroesUncasted.push(new Hero('Minute Hand', "minute_hand", false, Summand.comedy, Summand.fan));
            this.heroesUncasted[this.heroesUncasted.length - 1].specialHero = SpecialHero.minute;
            this.heroesUncasted.push(new Hero('Suburburn', "suburburn", false, Summand.dialog, Summand.lowStakes));
            this.heroesUncasted.push(new Hero("Talkster", 'talkster', false, Summand.dialog, Summand.new));
            this.heroesUncasted.push(new Hero("Firefist", 'firefist', false, Summand.fan, Summand.lowStakes));
            
            // villains
            this.villainsUncasted = [];
            this.villainsUncasted.push(new Hero("Hot Dog Hound", 'dr_mind', true, Summand.comedy));
            this.villainsUncasted.push(new Hero("Bombo", 'bombo', true, Summand.action));
            this.villainsUncasted.push(new Hero("Dr Mind", 'dr_mind', true, Summand.dialog));
            this.villainsUncasted.push(new Hero("Simple Thief", 'dr_mind', true, Summand.lowStakes));
            this.villainsUncasted.push(new Hero("Planetbane", 'dr_mind', true, Summand.bigStakes));
            this.villainsUncasted.push(new Hero("Obscuro", 'dr_mind', true, Summand.fan));
            this.villainsUncasted.push(new Hero("Miss Cast", 'dr_mind', true, Summand.new));

            this.villainsUncasted.push(new Hero("The Fartist", 'demon', true, Summand.comedy, Summand.action));
            this.villainsUncasted.push(new Hero("Paint Killer", 'paint_killer', true, Summand.comedy, Summand.lowStakes));
            this.villainsUncasted.push(new Hero("Jester", 'jester', true, Summand.comedy, Summand.bigStakes));
            this.villainsUncasted.push(new Hero("Prof Mind", 'dr_mind', true, Summand.dialog, Summand.dialog));
            this.villainsUncasted.push(new Hero("Dark Potato", 'dr_mind', true, Summand.action, Summand.action));
            this.villainsUncasted[this.villainsUncasted.length - 1].specialHero = SpecialHero.darkPotato;
            this.villainsUncasted.push(new Hero("Human Battery", 'dr_mind', true, Summand.new, Summand.new));


            this.villainsUncasted.push(new Hero("Mighty Fool ", 'jester', true, Summand.comedy, Summand.comedy, Summand.bigStakes));
            this.villainsUncasted.push(new Hero("Father Fart", 'demon', true, Summand.comedy, Summand.action, Summand.action));
            this.villainsUncasted.push(new Hero("The Consul", 'demon', true, Summand.dialog, Summand.dialog, Summand.bigStakes));
            this.villainsUncasted.push(new Hero("Evil Street Punch", 'demon', true, Summand.dialog, Summand.lowStakes, Summand.lowStakes));
            this.villainsUncasted.push(new Hero("Movie Maniac", 'demon', true, Summand.action, Summand.new, Summand.new));


            this.rng = new Phaser.RandomDataGenerator([date]);
            this.initialize();
        }

        initialize()
        {
            this.heroes = [];
            this.villains = [];
            for (let i = 0; i < this.initialHeroes; i++)
                this.castRandomHero(false);
            for (let i = 0; i < this.initialVillains; i++)
                this.castRandomHero(true);
        }

        castRandomHero(castVillain: boolean): Hero //output false if run out of heroes
        {
            let uncasted = this.heroesUncasted;
            if (castVillain)
                uncasted = this.villainsUncasted
            if (uncasted.length == 0)
                return null
            let addition = this.rng.weightedPick(uncasted);
            uncasted = uncasted.filter((hero, id, restArray) => hero != addition);
            if (castVillain) {
                this.villains.push(addition);
                this.villainsUncasted = uncasted;
            }
            else {
                this.heroes.push(addition);
                this.heroesUncasted = uncasted;
            }
            return addition;

        }
    }

}