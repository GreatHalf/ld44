module MyGame {

    export class Credits extends Phaser.State {

        background: Phaser.Sprite;
        music: Phaser.Sound;
        // player: MyGame.Player;

        create() {

            let creditYPos = 150;
            let creditOffset = 20;
            let creditSize = 30;

            this.game.stage.backgroundColor = '#f3f0ed';

            this.music = this.add.audio('creditsMusic', 1, true);
            this.music.play();

            // Title
            let creditLabel = this.game.add.text(5, 40, 'Credits', {font: creditSize*2 + 'px Arial', fill: '#ff0000'});
            creditLabel.x = (this.game.world.width - creditLabel.width) * 0.5;

            // Define Offset
            creditOffset += creditLabel.width*0.5;

            // Idea
            this.game.add.text(5, creditYPos, 'Ludum Dare 44 entry', {font: creditSize + 'px Arial'});
            let ideaLabel = this.game.add.text(5, creditYPos, '', {font: creditSize + 'px Arial'});
            ideaLabel.x = (this.game.world.width - ideaLabel.width) * 0.5;
            creditYPos+= creditOffset;

            // Main Developers
            this.game.add.text(5, creditYPos, 'Developer:', {font: creditSize + 'px Arial'});
            let programmingLabel = this.game.add.text(5, creditYPos, 'halfmule', {font: creditSize + 'px Arial'});
            programmingLabel.x = (this.game.world.width - programmingLabel.width) * 0.5;
            creditYPos+= creditOffset;
            // Graphics
            this.game.add.text(5, creditYPos, 'Twitter:', {font: creditSize + 'px Arial'});
            let graphicsLabel = this.game.add.text(5, creditYPos, 'https://twitter.com/thehalfmule', {font: creditSize + 'px Arial'});
            graphicsLabel.x = (this.game.world.width - graphicsLabel.width) * 0.5;
            creditYPos += creditOffset;

            // this.game.add.text(5, creditYPos, 'Twitter:', { font: creditSize + 'px Arial' });
            let deleteAchievements = this.game.add.text(5, creditYPos, 'tap to delete achievements', { font: creditSize + 'px Arial' });
            deleteAchievements.x = (this.game.world.width - graphicsLabel.width) * 0.5;
            deleteAchievements.inputEnabled = true;
            deleteAchievements.events.onInputDown.addOnce((text) => { AchievementManager.DELETE(); deleteAchievements.setText("deleted! please restart", true); }, this);
            creditYPos += creditOffset;
            // Back
            let backLabel = this.game.add.text(5, creditYPos, 'Back', {font: creditSize*2 + 'px Arial'});
            backLabel.x = (this.game.world.width - backLabel.width) * 0.5;
            backLabel.inputEnabled = true;
            backLabel.events.onInputUp.add(this.back, this);
        }

        back() {
            this.music.stop();
            this.game.state.start('MainMenu');
        }


    }

}