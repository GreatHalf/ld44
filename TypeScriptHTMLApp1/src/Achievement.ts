module MyGame {

	export class AchievementManager {

	   static LIST: Achievement[] = [];
       static MADE_ACHIEVEMENTS_ALREADY = false;
       static ACHIEVEMENTS_WERE_SAVED = false;
		static FILL() {
		    AchievementManager.LIST = [];
            AchievementManager.MADE_ACHIEVEMENTS_ALREADY = true;
            // if (localStorage.getItem('achievements') === null) {
                AchievementManager.LIST.push(new Achievement("Got Started", "Make a movie.", -3));
                AchievementManager.LIST.push(new Achievement("Minimal", "A movie with just one support character", -2));
                AchievementManager.LIST.push(new Achievement("Get Noticed", "three support characters = new Hero!", -1));
                AchievementManager.LIST.push(new Achievement("TickTock", "A movie with 'Hour Hand' & 'Minute Hand'", 0));
                AchievementManager.LIST.push(new Achievement("Too popular", "Overwork Protagonist 'The Orphan'", 1));
                AchievementManager.LIST.push(new Achievement("Hall of Fame", "Produce 10 Movies", 2));
                AchievementManager.LIST.push(new Achievement("Classico", "Protagonist Smash Potato vs Dark Potato", 3));
                AchievementManager.LIST.push(new Achievement("Torch Passing", "Protagonist Not-Bird dies, support Not-Bird Jr.", 4));
                AchievementManager.LIST.push(new Achievement("Darkest Two Hours", "Kill off four heroes in a movie.", 5));
                AchievementManager.LIST.push(new Achievement("Iconic Hero", "Have the same protagonist 4 times.", 6));
                AchievementManager.LIST.push(new Achievement("Galaxy at Peace", "18 movies - no more villains", 7));
                
            //}
            if(localStorage.getItem('achievements') !== null)
            {
                let unlockBits = JSON.parse(localStorage.getItem('achievements'));
                let i = 0;
                for (let unlockBit of unlockBits)
                {
                    AchievementManager.LIST[i].solved = unlockBit;
                    i++;
                }
            }
            
       }
        static SAVE_ACHIEVEMENTS()
        {
            let i = 0;
            let unlockBits: boolean[] = [];
            for (let achievement of AchievementManager.LIST) {
                unlockBits.push(AchievementManager.LIST[i].solved)
                i++;
            }
            localStorage.setItem('achievements', JSON.stringify(unlockBits));
            AchievementManager.ACHIEVEMENTS_WERE_SAVED = true;
        }

        static DELETE()
        {
            localStorage.removeItem('achievements');
        }

	}

	export class Achievement
    {
        name: string;
        description: string;
        type: number;
        solved: boolean;
        constructor(name: string, description: string, type: number)
        {
            this.name = name;
            this.description = description;
            this.type = type;
            this.solved = false;
            console.log("New achievement made " + this.name);
        }
        
        checkToSolve(player: Player, protagonist: Hero, villain: Hero, ...support: Hero[])
        {
            if (!this.solved)
            {
                if (this.check(player, protagonist, villain, ...support))
                {
                    this.solved = true;
                    return true;
                }

            }
            return false;
        }

        check(player: Player, protagonist: Hero, villain: Hero, ...support: Hero[])
        {
            switch (this.type)
            {
                case -3: return true;
                case -2: return (support.length == 1);
                case -1: return (support.length == 3);
                case 0: 
                    {
                        let hourFound = false;
                        let minuteFound = false;
                        let searchArray: Hero[] = [];
                        searchArray.push(protagonist);
                        searchArray = searchArray.concat(support);
                        for (let hero of searchArray)
                        {
                            if (hero.specialHero == SpecialHero.hour)
                                hourFound = true;
                            if (hero.specialHero == SpecialHero.minute)
                                minuteFound = true;
                        }
                        console.log("we found hour " + hourFound + " and also minute " + minuteFound);
                        return (hourFound && minuteFound);  
                    }
                case 1:
                    {
                        return ((protagonist.specialHero == SpecialHero.theOrphan) && protagonist.getFatigue() >= 100)
                    }
                case 2:
                    {
                        return (player.moviesProduced == 10);
                    }
                case 3:
                    {
                        return (protagonist.specialHero == SpecialHero.smashPotato) && (villain.specialHero == SpecialHero.darkPotato);
                    }
                case 4:
                    {
                        
                        if (protagonist.specialHero == SpecialHero.notBird) {
                            console.log("found notBird protagonist. Kill off? " + protagonist.checkIsMarkedForDeath());
                            if (!protagonist.checkIsMarkedForDeath())
                                return false;
                            
                            for (let hero of support) {
                                if (hero.specialHero == SpecialHero.notBirdJr) {
                                    console.log("found notBird jr");
                                    return true;
                                }
                            }
                        }
                        return false;
                    }
                case 5:
                    {
                        // kill off all
                        let searchArray = [];
                        if (support.length < 3)
                            return false;
                        searchArray.push(protagonist);
                        searchArray = searchArray.concat(support);
                        for (let hero of searchArray) {
                            if (!hero.checkIsMarkedForDeath())
                                return false;
                        }
                        return true;
                    }
                case 6:
                    {
                        return (protagonist.protagonistCount == 4);
                    }
                case 7:
                    {
                        return (player.moviesProduced >= 18);
                    }
            }

        }

    }

}