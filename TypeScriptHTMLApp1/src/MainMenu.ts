module MyGame {

	export class MainMenu extends Phaser.State {

	    background: Phaser.Image;
		music: Phaser.Sound;
        achievementTexts: Phaser.Text[];


		create() {
            this.game.stage.backgroundColor = '#f3f0ed';

            this.background = this.game.add.image(0,0,'background');
            this.background.width = this.game.world.width;
            this.background.height = this.game.world.height;
            this.background.alpha = 0;

			this.music = this.add.audio('menuMusic', 1, true);
			this.music.play();

            let gameTitleLabel = this.game.add.image(0, 50,'title');
            gameTitleLabel.x = (50 + this.game.world.width - gameTitleLabel.width) * 0.5;

            let gameStartButton = this.game.add.image(0, 600,'start');
            gameStartButton.x = (50 + this.game.world.width - gameStartButton.width) * 0.5;
            gameStartButton.inputEnabled = true;
            gameStartButton.events.onInputUp.add(this.fadeOutToGame, this);

            let gameCreditsButton = this.game.add.image(0, 800,'credits');
            gameCreditsButton.x = (50 + this.game.world.width - gameCreditsButton.width) * 0.5;
            gameCreditsButton.inputEnabled = true;
            gameCreditsButton.events.onInputUp.add(this.fadeOutToCredits, this);

            

            /*
            let achievementButton = this.game.add.image(0, 380 + 160 + 60,'main_ach');
            achievementButton.x = (achievementButton.width) * 0.1;
            achievementButton.inputEnabled = true;
            */
            // achievementButton.events.onInputUp.add(this.fadeOutToCredits, this);


            this.game.sound.muteOnPause = false;


			this.add.tween(this.background).to({ alpha: 1 }, 2000, Phaser.Easing.Bounce.InOut, true);


            /*
            if (!AchievementManager.MADE_ACHIEVEMENTS_ALREADY) {
                AchievementManager.FILL();
            }

            this.generateAchievementTexts(achievementButton.x, this.game.height + 128);
            let achievementStartY = achievementButton.y +  achievementButton.height;
            for (let achievementId in this.achievementTexts) {
                this.add.tween(this.achievementTexts[achievementId]).to({y: achievementStartY + Number(achievementId) * 84}, 1000, Phaser.Easing.Elastic.Out, true, 2000 + 100 * Number(achievementId));
                let frame = 0;
                if (AchievementManager.LIST[achievementId].solved)
                    frame = 1;
                let checkbox = this.add.image(this.game.width - 240, -500, 'solved', frame);
                this.add.tween(checkbox).to({y: achievementStartY + Number(achievementId) * 84 + 4}, 1000, Phaser.Easing.Elastic.Out, true, 2000 + 100 * Number(achievementId) - 100);
            }
            */
		}

		fadeOutToGame() {

			let tween = this.add.tween(this.background).to({ alpha: 0 }, 100, Phaser.Easing.Linear.None, true);
			tween.onComplete.add(this.startGame, this);

		}

        fadeOutToCredits() {

            let tween = this.add.tween(this.background).to({ alpha: 0 }, 100, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.showCredits, this);

        }


		startGame() {

			this.music.stop();
			this.game.state.start('Match', true, false);

		}

        showCredits() {

            this.music.stop();
            this.game.state.start('Credits', true, false);

        }

        generateAchievementTexts(startX: number, startY: number)
        {
            this.achievementTexts = [];
            for (let achievement of AchievementManager.LIST)
            {
                console.log("generating");
                let newText = this.achievementTexts.push(this.add.text(startX, startY, achievement.name.toUpperCase() + "\n     " + achievement.description.toLowerCase(), {font: "18pt Arial", fill: "#000000"}));
            }
        }


	}

}