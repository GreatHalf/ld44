var MyGame;
(function (MyGame) {
    var SimpleGame = (function () {
        function SimpleGame() {
            //this.game = new Phaser.Game(800, 600, Phaser.AUTO, 'content', { preload: this.preload, create: this.create });
            this.game = new Phaser.Game(720, 1280, Phaser.AUTO, 'content', { preload: this.preload, create: this.create });
            this.game.state.add('Preloader', MyGame.Preloader, false);
            this.game.state.add('MainMenu', MyGame.MainMenu, false);
            // this.state.add('Level1', Level1, false);
            this.game.state.add('Credits', MyGame.Credits, false);
            this.game.state.add('BootJr', MyGame.BootJr, false);
            console.log(this.game.state.states.default);
            this.game.state.start('BootJr');
        }
        SimpleGame.prototype.preload = function () {
            this.game.load.image('logo', 'phaser2.png');
        };
        SimpleGame.prototype.create = function () {
            var _this = this;
            var logo = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'logo');
            logo.anchor.setTo(0.5, 0.5);
            logo.inputEnabled = true;
            logo.events.onInputDown.add(function () {
                _this.game.state.add('Boot', MyGame.Boot, false);
                _this.game.state.start('Boot');
            }, this);
            // console.log("Trying to start 'boot'");
            //this.game.state.start('Boot');
        };
        return SimpleGame;
    }());
    window.onload = function () {
        var game = new SimpleGame();
    };
})(MyGame || (MyGame = {}));
/* class SimpleGame {

    constructor() {
        this.game = new Phaser.Game(800, 600, Phaser.AUTO, 'content', { preload: this.preload, create: this.create });
    }

    game: Phaser.Game;

    preload() {
        this.game.load.image('logo', 'phaser2.png');
    }

    create() {
        var logo = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'logo');
        logo.anchor.setTo(0.5, 0.5);
    }

}

window.onload = () => {

    var game = new SimpleGame();

}; */ 
//# sourceMappingURL=app.js.map