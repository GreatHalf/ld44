﻿module MyGame {
    class SimpleGame {
        game: Phaser.Game;

        constructor() {
            //this.game = new Phaser.Game(800, 600, Phaser.AUTO, 'content', { preload: this.preload, create: this.create });
            this.game = new Phaser.Game(720, 1280, Phaser.AUTO, 'content', { preload: this.preload, create: this.create });


            this.game.state.add('Boot', MyGame.Boot, false);
            this.game.state.add('Preloader', MyGame.Preloader, false);
            this.game.state.add('MainMenu', MainMenu, false);
            this.game.state.add('Match', Match, false);
            this.game.state.add('Credits', Credits, false);
            console.log(this.game.state.states.default);
            this.game.state.start('Boot');
        }


        preload() {
            this.game.load.image('logo', 'phaser2.png');
        }

        create() {
            var logo = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'logo');
            logo.anchor.setTo(0.5, 0.5);
            // console.log("Trying to start 'boot'");
            //this.game.state.start('Boot');
        }

    }

    window.onload = () => {

        var game = new SimpleGame();

    };
}



/* class SimpleGame {

    constructor() {
        this.game = new Phaser.Game(800, 600, Phaser.AUTO, 'content', { preload: this.preload, create: this.create });
    }

    game: Phaser.Game;

    preload() {
        this.game.load.image('logo', 'phaser2.png');
    }

    create() {
        var logo = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'logo');
        logo.anchor.setTo(0.5, 0.5);
    }

}

window.onload = () => {

    var game = new SimpleGame();

}; */